---
title: "Why Git flows is the Devil"
date: 2017-06-24T10:16:08+02:00
draft: false
toc: false
images:
tags:
  - git
  - strategy
  - ci
---


Actually, it's not really the Devil, but there are a couple of arguments for not using it in a modern fast paced CI development project and there are others arguments why you should use it, but those arguments are, at least to my mind not really good reasons and should be discarded, for the good of the entire industry.

## Where we come from
In the days of yore, basically 15 years ago, systems like CVS were common and Subversion was just seeing the light of day. Subversion was seen as an upgrade to CVS and in many ways it was, but there is a reason why Torvalds got frustrated and invented Git. I do not claim to know his entire reasoning, but one major factor was distribution. Systems like CVS and Subversion work on the premise that the code is stored in a single location and that all local work is merely a shadow of the real source code. This has two major drawbacks, the first being that the source code is very sensitive to disk and ram errors, such errors could corrupt the code and even make it completely useless.
The second big factor is the ability to work offline. In a centralized system, it's not really possible because the local checkout is nothing but a shadow of the code, in order to commit you would need to be online and connected to the repository. This is also of some importance in software projects where the team is distributed and work in different locations, but I digress, this is about git flows and not some decrepit old legacy system.

## Branching
In systems like CVS, branching was expensive because both branching and merging took time, it required planning, it required time and even then, it was considered hard work. With the introduction of Git, every developer got their own branch. Since Git is distributed and every clone of the repo is a repo in itself, it also means that every clone effectively has its own branches. This lead to the unfortunate statement that branching is cheap in Git.

## Merging is not branching
"_branching is cheap in Git_"

This of course is quite different from saying that merges are cheap. Someone of course thought it was the same thing and wrote a piece of garbage about how they achieved a successful branching strategy with Git. This strategy was called Git flows and it is about the worst thing I have ever read on the subject of version control. From what I can tell, the strategy was based on a work flow used by a horribly defective way of working, in an organization that don't know the first thing about successful software development.

## Why you would need git flow
* Because your way of working is very disorganized
* Because you like to spend half your time solving issues with issues and merging
* Because you like to pay salaries to SCuMs
* Because you have a need to over-complicate things
* Because you like micro management
* And many other equally dysfunctional reasons

Do yourself a favor now a do a Google image search for the term git flows, see anything you like?

## Why you shouldn't use git flows
* Because you understand that merges takes time and effort
* Because you want to work with continuous integration
* Because you work in a structured way
* Because you want to spend your time actually developing software
* Because you want to streamline development
* And many other equally good reasons

## The concern
My main concern with git flows is the time to integration. The longer time you wait to integrate your changes with anything, the longer lead time you will have before getting feedback on your changes. Although time cannot adequately be represented by how far away you are from your integration branch, it gives you some idea. In git flows, the actual development is done very far away from master, which would normally be your integration branch, this means that you will have a longer lead time before getting your changes through test and every correction you do, will again have a long lead time before it gets tested. This behavior is bad both from a developer perspective and a testing perspective. During initial development, testers are waiting for something to test, and during the end of development, the testers are usually booked solid testing everything before release, while development are idling, waiting for feedback.

Some clever manager will probably tell you that this is simply not true, during initial development, testers have other things to do, but curiously enough, those activities are either never clearly specified, or they have little to do with feature testing. Also, developers are of course never suppose to be idling, waiting for test, they're supposed to pick up the next feature and start with that. That is of course a noble idea, but it never works, meaning it fails because the process of software development is not clearly understood. The answer to this is of course CI and shorter lead time between development and test, faster or even instant feedback loops.

My second concern is that the whole branching model is completely useless for anything than completely dysfunctional software development. It adds no value whatsoever and over-complicates the work of developers, testers, SCMs and everyone involved in development, testing, release and deployment.

My third and actually last concern is that it actually increases the chances of errors being introduced, like pushes to dead branches, time consuming merges, with follow-up errors due to mistakes during merges and integration of downstream features.
