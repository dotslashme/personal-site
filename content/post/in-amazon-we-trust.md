---
title: "In Amazon We Trust"
date: 2018-01-22T08:00:57+01:00
draft: false
toc: false
images:
tags:
  - amazon
  - iaas
---


Recently I was browsing through my [LinkedIn](https://linkedin.com) flow and I have some [Amazon IaaS](https://aws.amazon.com/) evangelists in my contact list. One of them posted a meme where the message was "Customer decided to move to AWS - finally we're making progress", but it made me think if the move is the right one for that customer. This is in no way any criticism against Amazon, their service is great and I use it myself on a daily basis, but more a criticism against adopting technology simply because it's new and cool, despite having no actual use case to justify the move.

## The silver bullet
During my years as a developer, I have come across technologies that all promised to solve _everything_, tools known as the silver bullet. As I'm sure most of you realize, no tool can do everything well, the [UNIX principle](https://en.wikipedia.org/wiki/Unix_philosophy) has proved this beyond all shadow of a doubt. Tools that try to do everything ends up doing everything extremely poor, being bloated with functionality that hardly anyone ever use and as such, they generally suck big-time. Small, atomic or modular tools tend to do one thing really well, have superior performance and tend to have a more consistent behavior.

## Pushing for the silver bullet
The pushing for a technology like [IaaS](https://en.wikipedia.org/wiki/Infrastructure_as_a_service) is the same kind of thing, an [IaaS](https://en.wikipedia.org/wiki/Infrastructure_as_a_service) doesn't actually solve all your problems, it solves the problem of having an infrastructure, but it also comes with its own set of issues, like being a specific infrastructure. If your infrastructure matches that of your [IaaS](https://en.wikipedia.org/wiki/Infrastructure_as_a_service)-provider, then switching over is quite simple, but most people pushing for an upgrade to a [IaaS](https://en.wikipedia.org/wiki/Infrastructure_as_a_service)-provider lack both the technical skills to evaluate if the provider is a good fit and they usually make a big fat bonus for every client they migrate.

## Evaluation
Before you even think of doing a switch to a cloud platform provider, look at what benefits you will get. Will you gain anything by moving, beyond being able to say that your infrastructure and applications are hosted by such and such provider? Examine the cost, cost can be very tricky to calculate and without proper metrics in your current solution, this calculation can differ quite a bit from the reality. Cloud hosting can become very expensive very fast and the cost calculation is usually very different from non-cloud provided hosting.

Take geographical locations into account, do you need to host applications running in certain places of the world? If so, make sure the services you need are available in that region. The service offer usually vary with locations, especially with hosting in China. Finding out at a later date that a particular service is not available in a specific region could devastate your solution and force you into a working flow of maintaining a special-case-product.

Take migration into the cost, remember that moving everything unto a cloud provider might force you to do a complete setup of infrastructure again. Does your OPS team have the resources and know-how to take this step? Does your cloud provider support the kind of processing power you need? One very important thing to remember is that database power in the cloud is usually far less than it is on the ground. Are you using MQ technologies? Usually the same kind of limitations apply for MQ as for databases, high I/O and demanding workflows can trigger huge costs or in some cases not even be supported by your provider. Bogging down your MQ performance can be disastrous to the performance of your application.

## Conclusion
Use the right tool for the job, this means that if someone is pushing for cloud infrastructure hosting, make sure they have covered the basics before taking the leap. IaaS is cool, but make sure it's right for you.