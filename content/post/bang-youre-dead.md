---
title: "! bang you're dead"
date: 2017-05-10T19:25:28+02:00
draft: false
toc: false
images:
tags:
  - java
  - clean-code
  - best-practices
---


In all code writing, there are quirks, shortcuts and disciplines that should absolutely be shunned as far as possible. One of these really bad practices is using the bang sign (!) to negate something, and besides the obvious, being hard to see at a quick glance, negations can impact readability quite a bit. Reading code should be fluent for several reasons, but mainly because interruptions or hiccups in the thought process will break the current train of thought.
Let's look at some examples

```java
if (!isContextPopulated) {
	// Initialize context
}
```

Now be honest, if that comment weren't there, would you immediately grasp it's meaning, or would you have to read that if statement more than once?

Let me improve the readability a bit

```java
if (isContextEmpty) {
	// Initialize context
}
```

That's simpler to read, it's more fluent and you don't have to re-read it to understand it. If you did, you possibly think you missed the bang sign in front of it.

## The worst case scenario
Have you ever come across something like this?

```java
if (!isNotEmpty) {
	
}
```

This is a huge deal, you have to engage the logic center of the brain and interpret what it means, causing you to lose the train of thought immediately. Ultimately a huge time waster and a source of frustration.

## Conclusion
The use of Boolean logic is great, but learn to use proper names for your variables. Name them for their intended use and if you find yourself writing the bang sign before the variable in your if's, rethink your name. If in doubt if it's readable or not, leave the code until next day and see if you can read it like a book.
