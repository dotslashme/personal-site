---
title: "The Way of the Agile Warrior"
date: 2021-08-23T09:45:31+02:00
draft: false
---

For years, a good friend of mine have been badgering me about my views on agile development and so said and done, here they are.

## The wet dream

The project managers equivalent of a software developers wet dream of tearing down a legacy application, rebuilding it with a new cool framework and then have it magically working wonders and have clean, maintainable code, would be to have a framework of principles that allowed us to deliver perfect software in an organized and timely manner, without breaking the budget.

## We're going to use XXX

Whatever word you replace above, I'm sure, just as me, you've heard it and thought: "_Oh, here we go again. We're going to stick to our current way of working, but we're going to call it something new_".

## Why is it hard?

There's not a single explanation for this but an often recurring one, is that the organization have requirements that fit poorly with the agile principles.

In case you don't know the principles of agile software development, they are as follows:  
_**Individuals and interactions** over processes and tools_  
_**Working software** over comprehensive documentation_  
_**Customer collaboration** over contract negotiation_  
_**Responding to change** over following a plan_

According to the [agile manifesto](https://agilemanifesto.org/), where these four core values were stated, the left-hand side of each sentence is higher priority than the right-hand side.

Taking these four core values and looking into most organizations, you will find that they tend to fit poorly into most places, but the really sad part is that they tend to fit poorly due to an inability to change. Unless you have a legal requirement that forces you to conform to the right-hand side, you are making up excuses not to implement agile properly.

## An analysis of the core values

**Core value 1** - _Individuals and interactions over processes and tools_  
My personal view of this one is that if any tool or process hinders, delays or stops people from working effectively, it should simply be abandoned or disused. For me personally, this is huge. Being a massive Linux nerd, running custom hardware and being used to multiple customizations, being forced to use Windows as my main OS would literally make me very ineffective.  
How about a ticket system? Versioning software? Just for fun, list the platforms, processes and softwares you use on a daily basis. How many of them are the choice of your team? How many of them do you personally agree with? Do you have the freedom to remove or replace tools you don't like?

**Core value 2** - _Working software over comprehensive documentation_  
In most teams I have worked on, this has been a war zone with developers and tester on one side, arguing that we don't need any documentation, while managers have been on the other side, arguing that we need to document everything.  
In reality, you should not document how your software works, the software itself should be the documentation, ever living and ever changing, the same goes for tests, but that doesn't mean you don't need documentation.

I am personally a big fan of structured and readable code and that concept falls nicely in with undocumented code, because if your code is both readable and structured, you don't need documentation to explain how it works. If you're providing an API, it should have API documentation, meaning what goes in and what comes out, but the inner workings should have no need for explanation.

What you actually need to document is really up to your team and depends largely on your product and your needs. Usually you will need some sort of release notes, which can usually be automated if you have commit discipline.  
Architectural decisions might be another thing you need to document, in order to provide developers with guide lines for how to solve common problems and repetitive tasks.

Whatever you decide to document, remember that you should also have the right to question the practice of such documentation.

**Core Value 3** - _Customer collaboration over contract negotiation_  
In a lot of places where I have worked, this concept has been severely misunderstood. To have customer collaboration means a lot of things. First of all, it means the team is working towards what the customer actually wants, within the limits of the agreed upon constraints of the project. Yes, I know that sounds abstract, so let me explain it. What the customer actually wants are not really requirements. You have probably been working on epics and stories, so you think that is what the customer wants, but in reality, what the customer wants is your sprint goal or your demo goal.  
**Sprint goal is what should be achieved after the finish of the sprint, like:** _Being able to do a successful login to the web app, without support for any error flows_.  
**The demo goal sounds similar:** _to be able to navigate to the login page, enter a valid authorized email and password, press the login button and then being directed to the logged in page_.

This is what the customer actually wants, provided the customer was involved in creating those goals, which they obviously should be. The epics, stories, tasks, subtasks, etc are actually someones way of trying to understand these goals, by chopping it up into smaller pieces.

Epics and stories are by nature more contractual and they should be guided by the sprint and demo goal, not be seen as absolutes.

**Core value 4** - _Responding to change over following a plan_  
This seems hard to get wrong, because it keeps telling us to not plan so much, or not too far into the future, so that we will be able to better accommodate sudden changes. However, looking at "agile" practices in most software organizations, we find that they indeed tend to plan minutely, and even try to plan for the distant future.

The reason we shouldn't plan too far ahead, or in too much detail, is simply because plans change. Sometime, the details change. At other times, the whole plan falls apart and if you are planning too far into the future or in too much detail, accommodating any change becomes hard work.

## So we are not actually being agile?

If you are working somewhere where you are forced to use certain tools and processes, where you need to document your software and write other manual documentation, where your work specification is an epic or a story and you don't have either a sprint goal or a demo goal in place and your planning have deadlines, restrictions on change and plans extend longer than 2 weeks into the future, than yes, you are basically not agile, because you have adopted most of the right-hand side of the core values.

## A fifth core value

There is really not a need for it, other than to highlight how to behave toward changes, but I would propose we add the following core value:  
_**Question current practices** over adopting them_.

## Conclusion

A good way forward is to write justifications, kind of like user stories, for each practice you want to put into place, like:  
_As a manager, I want the current developers to document architectural decisions, so that developers have a decision guide to use for repetitive tasks and common problems_.

If you're like me, you would counter with a snippet library that the developers can use straight out of their IDE instead, removing the need to document it.
