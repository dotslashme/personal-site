---
title: "The why of kata"
date: 2017-05-25T08:55:14+02:00
draft: false
toc: false
images:
tags:
  - java
  - education
  - best-practices
---


As a martial artist, I'm deeply familiar with something called a kata. It is the ultimate tool for a martial artist to practice the art of their style. The kata is a series of basic movements from the particular style of martial art, put together in a visually pleasing form, intended to be practiced over and over again, until the movements become second nature. This repetitive training will with each iteration allow the martial artist to discover the true nature of the movements, as well as help them discover their own inner artist.

## Repetition, Repetition, Repetition
Someone, somewhere once said that it takes 10,000 hours of doing something to became good at it.

Thomas Edison famously said "_I have not failed. I've just found 10,000 ways that won't work._" when asked about the invention of the light bulb.

This philosophy is at the heart of the kata, to repeat the same movements over and over again, until only the most perfect way of doing that movement remain. Thus, the artist have perfected that movement in their own personalized style.

## Code kata
As you may have gathered, I'm a fan of katas, the code kata is no exception. It is a very good training tool for any software developer. Doing a code kata is simple, choose a task, it should be simple, like Fibonacci numbers or a Sudoku solver. Set up a time frame to do it, it should not take more than an hour to solve. Sit down, sketch it up, use pseudo code or whatever tools you like to design it, then implement it. When you're done, analyze the solution yourself and also share it with a friend or co-worker. Let them critique the solution and give you feedback. Do it again, same time frame, but with the critique in mind. Did it get better? Most likely it did. Let me show you an example.

## Fibonacci then and now
When I started out, I most likely would have done the Fibonacci sequence like this in Java:
```java
public int fibonacci(int n)  {
	if(n == 0) {
		System.out.println(0);
	}
	else if(n == 1) {
		System.out.println(1);
	}
	else if (n >= 2 && n <= 10) {
		System.out.println(return fibonacci(n - 1) + fibonacci(n - 2));
	}
}
```


Today, I would most likely do the same method this way:
```java
public void printFirstTenFibonacciNumbers() {
	int n1=0, n2=1, n3, i, count=10;
	System.out.println(n1);
	System.out.println(n2);

	for(i=2; i < count; ++i) {
		n3=n1+n2;
		System.out.println(n3);
		n1=n2;
		n2=n3;
	}
}
```

## The critique
The first method looks pretty don't you think, so sleek and well defined to the second one, so why did I evolve into doing the second way? Well, the first way uses recursion, something you learn in school and then you show it off with great pride, until you learn that Java actually sucks at recursion. When dealing with only the first ten numbers of the Fibonacci sequence, it may not show a significant performance slowdown, but with every iteration, the recursion takes longer and longer time, making this method really slow after a while.

It also uses if statements, representing branches of your code and with every branch, testing the method becomes more challenging. In a method this small, it may not really be an imposition to test, but the more complex methods you write, this becomes a bigger and bigger factor.

## The improved version
Here we just print the first two numbers, because we already know what they are, they require no calculation, so we skip the code branches and just execute the known factors. The other numbers will have to be computed, so we use a for loop to do it. No more recursion to slow us down, but it can still be improved. The number variables can be renamed to something sensible for instance or we could use doubles instead of ints, which would allow us to print Fibonacci numbers higher into the sequence.

## Conclusion
The kata will allow you to examine your code, let you evolve and become a better software developer, not to mention, the exercise will massage the gray cells and keep that old noodle of yours working. Work on tiny problems over and over again, find problems that pertains to your field, maybe do a REST service, try out a pattern or something else. Working on it again and again will allow you to quickly become very good at doing it right the first time.
