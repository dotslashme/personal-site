---
title: "Oh My Curly Braces"
date: 2017-12-20T07:12:04+01:00
draft: false
toc: false
images:
tags:
  - java
  - best-practices
  - strategy
---


Okay, let's face it, developers as a group is a whiny bunch of children, arguing over where to place the curly braces, if there should be space after an opening parenthesis and a whole other bunch of equally stupid stuff. Whenever we as a team or as a group come up with a standard for how our code is supposed to look, someone is always dissatisfied and 2 weeks down the road, the code style will start to diverge from the adopted standard. In this post, I will talk about remedies to this problem.

## The problem

The problem is not only that we're a whiny bunch of idiots, but we also have something known as muscle memory. That means we have a personal preference of how code is supposed to be written and during many coding hours, this preference gets hammered into us and it's hard to reprogram this habit. We instinctively press down on the keys like we always have and it is hard to change this behavior, because it requires us to think and evaluate as we go, not the desired state of programming.

## Why is is bad if developers have different styles?

Let me answer that with another question, have you ever used diff? Diff is a tool to compare two objects, usually files and the tool will output the difference between the two. That output is invaluable to show the changes occurring over history in a class for instance. Every commit can be studied and the change can be seen clearly as we browse through versions. The output from diff however can be a bit challenging to read, that's probably why there are numerous tools to simplify the visualization of changes.

Ideally, we would like diff to show us what code was added or removed, so we can easy browse through the history and see what each commit did, but now take into account that you have five developers and they all have their own style. What will diff show you? It will show you changes that have nothing to do with the change itself. It will show you a lot of curly braces moving back and forth, spaces added, spaces removed and so on. It will basically drown out what really changed in a lot of white noise.

## How do you solve it?

We're programmers right? How about we be lazy about this and try to automate the code standard so we can write code however we like to, but when we commit it, it will reformat everything to conform to the code standard.

The steps below is a rough outline of what you need to do

 - Agree to a code standard. Use Eclipse to format the code into something you can all agree on. Yes I said Eclipse specifically, that's because the code formatting settings in Eclipse is more detailed than in most other tools, including IntelliJ.
 - Test the code standard. Reformat a couple of files to make sure all things look the way you expect them to and still behaves the way they should. Some things, like XML does not play well with line breaks for example.
 - Export the code format settings from Eclipse. There's an export function for this in Eclipse and the file can be used in IntelliJ as well, through the use of a plugin. Save the file somewhere where all developers can reach it.
 - Enforce the use of the code standard. The easy way is just to tell developers to install the necessary plugins and start re-formatting the code before commit, but there are other ways to really enforce it as well. Git-hooks can be used to force a re-format before the commit is done and you can also use static and dynamic code analysis to refuse commits.

## Conclusion

The use of a code standard is not to make your life as a developer harder, it is about history and the ability to take educated decisions based on that history. Tracking changes through the lifetime of an application is hard, but it becomes a lot more difficult when you introduce white noise as changes. Reducing the white noise in your changes will aid and help you take more informed decisions in a shorter time.