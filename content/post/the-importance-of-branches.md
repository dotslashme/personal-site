---
title: "The Importance of Branches"
date: 2017-12-31T09:09:11+01:00
draft: false
toc: false
images:
tags:
  - development
  - best-practices
  - testing
  - python
---


In this posting I will discuss how your code style and flow control impacts your test cases. I will start with a typical bad example, having lots of code branches and then refactor it down, showing how testing is impacted along the way.

## Flow control

If you're not familiar with the term flow control, I suggest you read [the flow control guide](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/flow.html), but in essence flow control consists of <code>if</code>, <code>switch</code>, <code>for</code>, <code>while</code>, <code>do</code>, <code>continue</code> and <code>break</code>.

In this post I will focus solely on the <code>if</code> flow control and its impact on test cases.

## The code

Today I was reviewing python flow control in a [Python3 course](https://www.udemy.com/python-the-complete-python-developer-course/) by [Tim Buchalka](https://twitter.com/timbuchalka) and one of the examples he was using is perfect to show off how flow control can have a great impact on your testing. Although this is not an exact replica of the code he used, but it uses the same principles.

```python
guess = int(input("Guess a number between 1 and 10: "))

if guess > 5:
    guess = int(input("Sorry, that's incorrect, please guess lower: "))
    if guess == 5:
        print("You guessed correctly")
    else:
        print("Sorry, that's incorrect")
elif guess < 5:
    guess = int(input("Sorry, that's incorrect, please guess higher: "))
    if guess == 5:
        print("You guessed correctly")
    else:
        print("Sorry, that's incorrect")
else:
    print("Wow, you guessed it right on the first try")

```

If we think about this code in testing terms, we can easily describe it using a flow chart. The starting point of course being user input and the finishing point being the print statement, but let's focus on what's going on between those two points.
![Description of flow chart: Input divides into three branches, one where the input is lower than five, thus referred to as less-than-branch, a second one where the input is higher than five, thus referred to as greater-than-branch and the final one where input is equal to five, thus referred to as hole-in-one-branch. Input is first read, then evaluated by if/elif/else. From there the three branches emerge. The less-than-branch prints an error message and asks the user to guess again, this time giving the hint that the sought after number is higher than the previous input. Input is then checked again and if it is equal to five, a success message is printed, else a failure message is printed. The greater-than-branch prints an error message and asks the user to guess again, this time giving the hint that the sought after number is lower than the previous input. The new input is then checked and if it's equal to five, a success message is printed, else a failure message is printed. The hole-in-one-branch is triggered when the user input is five and a special success message is printed.](/img/the-importance-of-branches/test_tree_complex.png)

We have a single starting point, but we have five end states, three of them indicating a good outcome and two of them indicating a failure. However, they are not produced by the same line of code, so we don't have a good atomic structure with a single input and a single output. Let's have a look at the impact on testing.

## Testing the above solution
Testing uses a mathematical term called coverage, I'm not going to go into it now, because it is quite complex, but the only thing we need to concern ourselves with when it comes to testing, is that our tests cover all branches.

A branch is represented in code by a decision. Every time you do a decision somewhere in your code, you create a branch. Branch here is a term used to denote an alternative route through your code, in order to reach the endpoint.

In the code above, we can see three major branches to begin with, the three cases are: <code>input &lt; 5</code>, <code>input &gt; 5</code> and <code>else</code>. From the first and second branch, we have further branches and in order to test this, we would have to implement the following tests:

- Input is 5
- First input is >5
    - Second input is 5
    - Second input >5
- First input is <5
    - Second input is 5
    - Second input is <5

Rewriting code that print things are in no way an easy task and taking into account that we here need to check the input value more than once, in order to give the user a second guess, does not simplify things, but after a rewrite, the code turned out like this:

```python
def success():
    print("Nice! You guessed it")


def failure():
    print("Sorry, that's incorrect")


def hole_in_one():
    print("Wow, you got it on the first try!")


def get_input(text):
    guess = int(input(text))
    return guess


guess = get_input("Guess a number between 1 and 10: ")

if guess == 5:
    hole_in_one()
else:
    failure()
    msg = None

    if guess < 5:
        msg = "Try a higher number: "
    else:
        msg = "Try a lower number: "

    guess = int(input(msg))

    if guess == 5:
        success()
    else:
        failure()
```

An initial inspection shows that we no longer have three branches from the input, but we still have some logic in our else, but inspection of the flow chart will tell the story better than me.
![Description of flow chart: Input divides into two branches, one where the input is equal to five, thus referred to as hole-in-one-branch, a second one where the input is not equal to five, thus referred to as else-branch. Input is first read, then evaluated by if/else. From there the two branches emerge. The hole-in-one-branch is triggered when the user input is five and a special success message is printed. The else branch prints a failure message, then evaluates if the input was in the low or high range by the use of another if/else. In these branches, an input hint is set to a message variable to prompt the user with guess lower/guess higher hint. Both branches converge into a single point to ask the user for another input, this time with the hint set in both the branches. After new input is given, another if/else evaluates if the guess was right or wrong and prints the corresponding message.](/img/the-importance-of-branches/test_tree_refactored.png)

The astute reader will of course say that there is no difference in the amount of test cases, we still have a lot of complexity in the <code>else</code> branch. Yes we do, but we have successfully replaced two of the branches with a single one, this simplifies the entry, that will now branch into two distinct branches. The logic within the else branch is not something we can do much about, without increasing the complexity further.

The most important improvement is however the exit points, where we now have put them into methods and as such we only have three exit scenarios to account for in our tests.