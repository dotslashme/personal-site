---
title: "Raise the Bar"
date: 2018-12-12T12:37:15+08:00
draft: false
toc: false
images:
tags:
  - rant
  - business-value
  - turd-brag
---


With few exceptions, the last six months of watching [LinkedIn](https://linkedin.com) has made me sick. The amount of stupid, turd-bragging and completely inappropriate posts can be summed up by a 28 second clip of South Park.

<iframe width="640" height="480" src="https://www.youtube.com/embed/QpjVfyEuqLU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

Making statements such as:

> The following examples are taken completely out of context and some have even been translated into English from their original language.

- _The bosses best friend is the new pro-pack with PowerPoint templates_
- _AI can now detect patterns in pedestrian crowd_
- _If a client don't want to meet you, bug them until they cave_

These are some of the things I have seen posted the last weeks and they make me sick and here's why.

If your content value is so low that you cannot present anything without a pro-pack of PowerPoint templates, then maybe you shouldn't bother presenting it in the first place. Sure, we all react to nice design and colors, but it's not a life preserver. Remember that old Internet saying: "_Content is king_" ? It still applies!

AI can now do pattern recognition, wow! Pattern recognition has been around for a long-ass time before AI and if you're just now getting pattern analysis from your AI, you're about 10 years behind conventional pattern recognition systems. We can all do the AI equivalent of the hello world, it's nothing new. Get over yourself!

Ah, the sales force equivalent of a horny teenage boy badgering a girl into spreading her legs. From personal experience I cannot even tell you how stupid this is. The argument is of course that you should talk to them of the business value you can add, but a clients refusal to meet you is most likely based on a bad rep, not that the fact that you have failed to provide a service in the past. Badgering them will not improve their view on you, respecting a no, however might.

So please flush that toilet, bragging about your polished turd does not make it any less of a turd. Raise the bar people!
