---
title: "Git Discipline"
date: 2019-01-17T12:11:20+08:00
draft: false
toc: false
images:
tags:
  - git
  - best-practices
---


Dealing with source control is something that every developer should know like the back of their hand. If you just looked at the back of your hand and spotted a mole you didn't know was there, you're in trouble, just like people not knowing how to deal with basic source control. This time I vent on people taking "*commit early, commit often*" a little bit too much to heart.

## The issue

Changing interfaces is always a pain in the ass, every change cascades into a number of changes downstream and doing such a change is fraught with dangers of breaking builds and other functionality, but with discipline it can be done without any issues. If the interface is only changed by you, the rest of the solution can be locked towards the last working version and you can change the interface in peace and quiet. Once all implementations are done, you can release a new interface version and tie your changes towards the new version.

This might work fine when you work in a small team or alone, but in a large organization with multiple teams spread out across the globe, it is a spectacularly bad idea and here's why.

You have no idea if there are others working on the same interface as you and what would be the consequence of you pushing interface changes without implementations? The answer is simple, you basically fuck up for everyone else. Pulling the new code will break and it will no longer build, simply because the implementations are missing. In order to work around it, developers are forced to either implement the interface methods with real or dummy-methods. However, committing your changes is completely out of the question, since you most likely will not be able to implement the methods correctly and even so, committing it will cause huge merge conflict for the developers that are working on it.

## The cure

Plain and simple - branches! When you need to work with other people, learn to respect them and show you have some discipline by using branches. Problem solved! By using branches the build will not break, others can continue working with the code without having any issues, since the problem is confined to a different branch.
