---
title: "What's in a name?"
date: 2017-07-11T13:47:25+02:00
draft: false
toc: false
images:
tags:
  - java
  - naming
  - best-practices
---


I hear a lot of developers complaining about naming and how to do it correctly. For those with a sense of humor, there's always the [enterprisify website](https://projects.haykranen.nl/java/), but I'm here to talk about what's in a name and why naming is important in so many ways. In other words, I'm here to talk business, despite the corny title.

## Why naming is hard
There's more than a single reason, but the most obvious reason is simple, the problem has not been broken down into fully understandable pieces. Let me show off with an example that most of you will be familiar with.

In this case, our customer has a use case where they will build a new rest endpoint that will accept <code>http post</code> and consume an incoming xml document with temperatures. The work to parse this would be ours and as such, probably the first thing we would do would be to create a class called <code>Parser.java</code>.
Already here, we have gone astray and our naming strategy is already way too broad. Let's back it up a bit and think through what we are doing.

Will this class parse multiple formats?
Will this class parse several different documents?

In our case, the answer to both these questions are no. So, we use that to improve our naming standard for the class into <code>ParseTemperatureXml.java</code>. Parsing XML can be quite verbose as many of you undoubtedly know and as we go along, we discover that our method for parsing this data becomes longer and longer, so we start breaking out snippets into private methods and refactor as we go.

Somewhere during the implementation we discover that the incoming xml declares the temperatures in Fahrenheit and we want to store them in Celsius. Well, we can't have that in our parser, now can we? Of course not, so we create another class for it, but what to name it? We don't know if we will encounter more of these strange things that might require conversions or other enrichments, so we decide to name the class <code>ParseTemperatureXmlHelper.java</code>. We then add a conversion method from Fahrenheit to Celsius.

After reading the specs again you find that the xml contain degrees specified as float values and we want to store them as integers, we don't require the precision and so you add another method to the helper class, I mean that's why it's there right?

Now imagine that some other developer want a class that can convert Celsius to Fahrenheit for some other use case. A quick search in the code base shows no classes that can do that, so a new class is created, because why should it be in <code>ParseTemperatureXmlHelper.java</code>? Had the class been called <code>TemperatureConverter.java</code> it would have been easier, but then where would we have put the method to deal with floats? Well in a class called <code>NumberConverter.java</code> of course.

## Simplification
Let me make it easier for you, if you heard someone fingered your girlfriend, would you like to go looking for him in <code>SomeGuyInTown.java</code> or in <code>TheGuyThatFingeredMyGirlfriend.java</code>?
