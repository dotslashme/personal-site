---
title: "Gitlab - Sometimes You Make Me Sad"
date: 2020-11-08T08:10:23+01:00
draft: false
toc: false
images:
tags:
  - git
  - best-practices
---

I'm a huge fan of GitLab, I think they offer an amazing product and I have recently started using it even more, but one thing that really makes me troubled is why a modern CI service offer Git flow implementation, oh sorry I meant GitLab flows.

If you haven't already, please read [Why Git flows is the Devil](/post/why-git-flows-is-the-devil/) for a rant about Git flow.

## The good

Reading [their own introduction to GitLab flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) shows that they understand the issues with Git flow, they then present the GitHub flow, which is a much simpler way of working, using a master branch, possibly feature branches, possible tags that off-shoots into hotfix branches for each tag. So far so good!

## Well, here come the flow

- environment branches
- cherry-pick into release branches

![NEIN! NEIN! NEIN!](/img/nein.gif)

Just stop, please I beg you. Take your responsibility as a modern CI/CD service, throw crap like this out the window like the yesterdays trash it is. Let me explain why.

More branches mean a higher risk of missing commits. Why? Well, it's simple isn't it. Every single commit needs to be cherry-picked into multiple branches, making the risk of omitting a commit a big possibility. With these branches you also need to `git log master ..environment-branch` for every single branch you have to find any missing commits. With the simplified branch strategy of using release branches, you only need to do a single `git log master ..previous-release-branch` before doing your release.

Don't cherry-pick into release branches, it is just plain wrong. A commit into a release branch suggests a hotfix to a release and the flow should always be to fix it in the release branch and then cherry-pick into master. What's the frickin difference you ask? Speed! Intent! Process! First of all, in a proper CI environment you would have a deploy machine/cluster tied to your release branch. In case of emergency, you could commit on the release branch and CI would roll instantly, also keeping master free of your commits until it has been confirmed to actually work (hotfixes tend to be messy). If another team is working on master, this will not cost them hours of lost work. When the hotfix has passed CI, you can release straight from the release branch and deploy, then you can in peace and quiet cherry-pick the working solution into master, making sure the fix is also available in the next release.

## Conclusion

I love GitLab, I will continue using it and still support it, but if our industry is to move forward we need to leave outdated and micro-managerial techniques where they belong - dead in a ditch!