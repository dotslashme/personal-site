---
title: "Local containerization with Kubernetes"
date: 2020-04-30T11:04:28+02:00
draft: false
toc: false
images:
tags:
  - virtualization
  - kubernetes
  - virtualbox
  - linux
  - lens
---


## VirtualBox

I am not going to go into a lot of details concerning VirtalBox, I'm going to assume you either know it or can read up on how to install/configure it on your system. I am however going to mention a couple of things. The first is that we are going to use Debian for this tutorial, so you might wanna swing by [their place](https://www.debian.org/distrib/) and download the amd64 installer ISO. The second thing is that the virtual machine you need to create should have at least two cores and 2GB RAM. Don't overdo it though, since we will clone this machine later, so leave yourself some resources to make sure your computer can run and you can still have two of these machines running at the same time. The last thing is to set the network mode to bridged adapter.

In my personal setup, I created a machine with 4GB ram, 2 cores and sound disabled.

## Debian

Next, it's time to install Debian and there are a couple of steps you need to configure manually to get this working.

- Manual networking. The networking option will go through with DHCP, but go back and reconfigure it manually to a static IP. The IP should be on the same subnet as the network adapter in your VirtualBox setup.
- User, please set up the same username as your local user on your host machine. This will make it simpler to ssh into the machine later on.
- Disk partitioning. These machines cannot have swap enabled, so instead of using the guided option, choose manual and configure a `/` mount point using ext4 that covers the whole of the VirtualBox disk. When you try to finish it will warn you that there is no swap partition configured, but this can safely be ignored.
- Lastly, in the package selection, make sure to mark `ssh-server` for installation. The rest can and should be unselected, since this machine will basically be headless.

After the machine has been installed, you will need to login to the machine once through the VirtualBox interface. Log in with your root user at this point so we can configure your local user with sudo permissions. Run the following commands to install sudo:

```bash
# apt-get install sudo
# usermod -a -G sudo vbox_user_name
```

Now, you can reach that machine through the terminal on your local machine with `ssh vbox_user_name@master_ip`.

## Docker install

Install docker by running the following on the vbox machine:

```bash
$ sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
$ sudo apt-get update \
    && sudo apt-get upgrade -y \
    && sudo apt-get install -y docker-ce docker-ce-cli containerd.io
```

## Kubernetes install

Install all Kubernetes tools using the commands below:

```bash
$ curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
$ cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
$ sudo apt-get update && sudo apt-get install -y kubelet kubeadm kubectl
$ sudo apt-mark hold kubelet kubeadm kubectl
```

```bash
$ sudo systemctl daemon-reload
$ cat <<EOF | sudo tee /etc/docker/daemon.json
{ 
    "exec-opts": ["native.cgroupdriver=systemd"]
}
EOF
$ sudo systemctl restart docker
$ sudo mkdir -p /var/lib/kubelet
$ cat <<EOF | sudo tee /var/lib/kubelet/config.yaml
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
EOF
$ cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
$ sudo sysctl --system
$ sudo systemctl restart kubelet

```

## Cloning the machine

Once these things have been done, shut the machine off either through the VirtualBox interface or by using `sudo shutdown -h now`

Go into the VirtualBox interface and clone the machine, but make sure to choose to generate new MAC address and then do a full clone. After it has been cloned, start it up.

We will need to ssh into the machine, but currently it will have the same ip-address as the previous one, so make sure to ssh into the machine with StrictHostKeyChecking enabled `ssh -o StrictHostKeyChecking=false vbox_user_name@master_ip`. Inside the machine, edit the `/etc/hostname` to something else and edit the `/etc/network/interfaces` and set the ip to the slave machine to something memorable in the same subnet. Also correct the `/etc/hosts` to the name you set in `/etc/hostname`.
Now reboot the machine with `sudo reboot`
Once the slave machine has rebooted, start master machine (the first one).

## Configure the control plane

SSH to your control plane machine and run the following command to create a new master

```bash
$ sudo kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=master_ip
```

This step might take some time, but should complete without issues. If you do have issues here, you might have not created virtual machines with at least 2 cores, less than 2GB RAM, failed to configure systemd driver for docker or kubectl.

### Make sure you can run kube as a normal user

This step is optional, you do not explicitly need it, but it is nice to have in case you want to manually execute kubectl commands at some point.
Run the following commands on the master server:

```bash
$ mkdir -p $HOME/.kube
$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
$ sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

### Install the flannel network plugin

You can choose from a multitude of different network plugins, but here you should use Flannel, mainly because we will run our pods in the 10.244.0.0/16 subnet, so make sure to execute:

```bash
$ kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```
## Join your slave to the cluster

When you configured the master with kubeadm init, the response was among other things a command you need to join your slave to the master. Copy this response and execute it on the slave machine:

```bash
kubeadm join 10.0.2.10:6443 --token xxxxxx.xxxxxxxxxxxxxxxx \
    --discovery-token-ca-cert-hash sha256:XXXXXXXXXXXXXXXXXXXXXXXX
```

If everything goes okay it should end with:

```bash
This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.
```

## Lens

On your local machine, you should now install lens in order to get a nice tool to work with your master and slave machines, so head on over to [their github page](https://github.com/lensapp/lens) and install their application on your local machine.

### Configure lens

In order to make lens work, we need to repeat the steps for running kube as a normal user on our local machine, so run the following commands on your local machine:

```bash
$ mkdir -p $HOME/.kube
$ touch $HOME/.kube/config
```

Edit the config file and paste the content from `$HOME/.kube/config` from the master machine, then save it.

Start lens and add your config that should now appear automatically in the drop-down list. That's it, you're done!