+++
title = "Resume"
date = "2014-04-09"
aliases = []
[ author ]
  name = "Christian Velin"
+++

Christian is mainly a backend developer with a strong focus on code quality, self-organisation and self-improvement. His background is mainly in J2EE monolithic applications and API development. Main interests are refactoring, modularization, containerization and boy scout coding.


## Education

- **Go: The complete bootcamp**  
*Udemy 2020*

- **Go: The complete developers guide**  
*Udemy 2019*

- **Using Python for offensive penetration testing**  
*EH Academy 2018*

- **Kubernetes course from a DevOps guru**  
*Udemy 2018*

- **Mastering Ansible**  
*Udemy 2017*

- **AWS Lambda and the serverless framework**  
*Udemy 2017*

- **Software configuration management**  
*Learning tree international 2008*

- **Advanced web programing**  
*Uppsala University 2004-2008*

## Experience

- **Software engineer**  
**[Fore development](https://fore.dev/)**  
**Aug 2021 -**

- **Software engineer**  
**[Benify](https://www.benify.se/)**  
**Apr 2021 - Jul 2021**

- **Software engineer**  
**[Omegapoint](https://omegapoint.se/)**  
**Apr 2017 - Mar 2021**  
My time at Omegapoint was spent in various projects in different roles, but my most prominent role was as software enginner with WirelessCar, but I have also done in-house work, providing technical expertise in dockerization, acting as a technical advisor to sales and mentoring of junior colleagues.

- **Software engineer**  
**[Cybercom](https://www.cybercom.com/)**  
**Sep 2014 - Mar 2017**  
Most of my time at Cybercom was spent at Ericsson, working as an SCM with a project called Connected vehicle cloud, but I was also briefly involved in a project at the Swedish tax agency, working with a EU project called MOSS. Alongside these responsibilities I also acted as a mentor to some of my junior co-workers.

- **Software engineer**  
**[Experis](https://experis.com/)**  
**May 2011 - Aug 2014**  
Most of my time with Experis was spent at Volvo trucks, developing new features and supporting two legacy systems, one developed in .NET and the other one in Java.

- **Software engineer**  
**[TicketMaster](https://www.ticketmaster.com/)**  
**Jan 2008 - Apr 2011**  
As part of the in-house developer team, I worked in a strong agile environment, tearing down their legacy web page application, replacing it with a modern web application using OO PHP and JavaScript.

## Projects

- **[Kubernetes kluster on bare metal](https://dotslashme.com/post/local-containerization-with-kubernetes/)**  
**2020**  
[A kubernetes kluster used for online storage and documentation](https://github.com/dotslashme/kubernetes-cluster).  
***Technology:*** Kubernetes, Linux, Traefik, Postgresql

- **[Volvo on call](https://www.volvocars.com/intl/why-volvo/human-innovation/future-of-driving/connectivity/volvo-on-call)**  
**2017 - 2020**  
Along with a small team, my main responsibility was to structure and drive the quality of the backend development, along with maintaining uptime and availability. Major achievements during my time there included a complete re-structure of their local development environment that was transformed and modernized using Docker and Linux, and the upgrade of their current REST API to use OpenAPI 3.0.  
***Technology:*** JBoss, J2EE, Spring, Oracle DB, Docker, Linux, Lambda, S3, DynamoDB

- **[In-Car delivery]()**  
**2016 - 2017**  
A brief side project during my time with the Connected vehicle cloud project, in which a suitable openID solution was to be set up, allowing third-party actors access to locked vehicles.  
***Technology:*** OAuth2, OpenID connect

- **[Connected vehicle cloud](https://www.ericsson.com/en/portfolio/iot-and-new-business/iot-solutions/iot-for-automotive/connected-vehicle-cloud)**  
**2015 - 2017**  
I was sourced as a software engineer, but with time the position was shifted more into a pure SCM-position, where me and a co-worker did a complete restructuring of their entire development and release process by introducing code standards, quality gates and an authorization program for commits and releases.  
***Technology:*** AWS, F5, Java, Spring, Oracle DB, Redis, Git, Gerrit

- **[ElectriCity](https://www.electricitygoteborg.se/en)**  
**2015**  
My time with this project was brief and I worked in an advisory capacity only, supervising the work of junior colleagues and gave advice concering implementations and strategy.  
***Technology:*** Java, ProtoBuf

- **[MOSS]()**  
**2014**  
A project mandated by EU, dealing with the harmonization of VAT taxation distribution within Europe.  
***Technology:*** C#, MsSQL

## Skills
{{< rawhtml >}}
<div style="flex-grow: 1; display: flex; flex-direction: row; text-align: center;">
	<span style="width: 25%">
		<i class="fab fa-java" style="font-size: 300%"></i>
		<p>
			Java
		</p>
	</span>
	<span style="width: 25%">
		<i class="fab fa-python" style="font-size: 300%"></i>
		<p>
			Python
		</p>
	</span>
	<span style="width: 25%">
		<i class="fas fa-code" style="font-size: 300%"></i>
		<p>
			Golang
		</p>
	</span>
	<span style="width: 25%">
		<i class="fas fa-code" style="font-size: 300%"></i>
		<p>
			Spring
		</p>
	</span>
</div>
<div style="flex-grow: 1; display: flex; flex-direction: row; text-align: center;">
	<span style="width: 25%">
		<i class="fab fa-docker" style="font-size: 300%"></i>
		<p>
			Docker
		</p>
	</span>
	<span style="width: 25%">
		<i class="fas fa-cubes" style="font-size: 300%"></i>
		<p>
			Kubernetes
		</p>
	</span>
	<span style="width: 25%">
		<i class="fas fa-database" style="font-size: 300%"></i>
		<p>
			SQL
		</p>
	</span>
	<span style="width: 25%">
		<i class="fas fa-cogs" style="font-size: 300%"></i>
		<p>
			DevOps
		</p>
	</span>
</div>
{{< /rawhtml >}}
